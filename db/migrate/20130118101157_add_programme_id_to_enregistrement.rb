class AddProgrammeIdToEnregistrement < ActiveRecord::Migration
  def change
    add_column :enregistrements, :programme_id, :integer
    add_column :enregistrements, :etat, :integer, :default => 0
  end
end
