class Programme < ActiveRecord::Base
  attr_accessible :debut, :fin, :libelle, :personne_id, :chaine_id
  has_many :personnes
  has_many :enregistrements, :dependent => :delete_all
  validates :libelle, :personne_id, :chaine_id, :presence => true
  belongs_to :chaine

  def chaine
  	Chaine.where('numero = ?',self.chaine_id).first.nom 
  end

  def personne
    if Personne.where("id = ?",self.personne_id).blank?
  	 ""
    else
      Personne.find(self.personne_id).nom_complet
    end
  end


end
