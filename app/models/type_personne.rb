class TypePersonne < ActiveRecord::Base
  attr_accessible :libelle
  validates :libelle, :presence => true
  has_many :personnes, :dependent => :delete_all
end
