#coding : utf-8
class Enregistrement < ActiveRecord::Base
  attr_accessible :marge_apres, :marge_avant, :programme_id, :etat
  validates :marge_avant, :marge_apres, :programme_id, :presence => true
  belongs_to :programme

  def etat_d
  	if Time.now < programme.debut - marge_avant.minutes
  	   "En attente"
    elsif Time.now > programme.debut - marge_avant.minutes and Time.now < programme.fin + marge_apres.minutes
	     "En cours d'enregistrement"
	   else
	     "Terminé"
	   end
  end

  def titre_tv
    programme.libelle
  end

  def chaine
    if Chaine.where("numero = ?",programme.chaine_id).first.img_link.blank?
      Chaine.where("numero = ?",programme.chaine_id).first.nom
    else
      ("<img src=" + Chaine.where("numero = ?",programme.chaine_id).first.img_link + " alt=" + Chaine.where("numero = ?",programme.chaine_id).first.nom + " title=" + Chaine.where("numero = ?",programme.chaine_id).first.nom + " style='width:70px; height:30px;' />").html_safe
    end
    
  end

  def heure_debut
    programme.debut.strftime('%d-%m-%Y à %Hh%M')
  end

  def heure_fin
    programme.fin.strftime('%d-%m-%Y à %Hh%M')
  end

  def rec_debut
    (programme.debut - marge_avant.minutes).strftime('%d-%m-%Y à %Hh%M')
  end

  def rec_fin
    (programme.fin + marge_apres.minutes).strftime('%d-%m-%Y à %Hh%M')
  end


  
end
