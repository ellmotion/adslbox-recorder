class Chaine < ActiveRecord::Base
  attr_accessible :nom, :numero, :img_link
  validates :nom, :numero, :presence => true
  validates_uniqueness_of :numero

  def en_ce_moment
  	#Test si il y a un film actuellement selon la date et l'heure, si oui, il l'affiche
  	if Programme.all(:conditions => ['debut < ? AND fin > ? AND chaine_id = ?', Time.now, Time.now, self.numero]).blank?
      "Rien pour le moment"
    else
      Programme.all(:conditions => ['debut < ? AND fin > ? AND chaine_id = ?', Time.now, Time.now, self.numero]).first.libelle
    end
  end

  def prochainement
  	#Test et recupère le film d'après que celui du moment ou si il n'y a pas de film en ce moment, il recupère le film le plus proche
	if Programme.all(:conditions => ['debut < ? AND fin > ? AND chaine_id = ?', Time.now, Time.now, self.numero]).blank?
      if Programme.all(:conditions => ['chaine_id = ?',self.numero]).blank?
        "Aucun Programme Prochainement" 
      else
        if Programme.where('debut > ? AND fin > ? AND chaine_id = ?', Time.now, Time.now, self.numero).order('debut ASC').first.blank?
        "Aucun Programme Prochainement"
      else
        Programme.where('debut > ? AND fin > ? AND chaine_id = ?', Time.now, Time.now, self.numero).order('debut ASC').first.libelle
      end
        # Programme.all(:conditions => ['chaine_id = ?',self.numero]).first.libelle + " le "  + Programme.all(:conditions => ['chaine_id = ?',self.numero]).first.debut.strftime('%d-%m-%Y à %Hh%M')
      end
    else
      if Programme.where('debut > ? AND fin > ? AND chaine_id = ?', Time.now, Time.now, self.numero).order('debut ASC').first.blank?
        "Aucun Programme Prochainement"
      else

      Programme.where('debut > ? AND fin > ? AND chaine_id = ?', Time.now, Time.now, self.numero).order('debut ASC').first.libelle
    end
    end
  end

  def logo
    if self.img_link.blank?
      self.nom
    else
      ("<img src=" + self.img_link + " alt=" + self.nom + " title=" + self.nom + " style='width:70px; height:30px;' />").html_safe
    end
  end
end
