class Personne < ActiveRecord::Base
  attr_accessible :nom, :prenom, :surnom, :type_personne_id
  has_many :type_personnes
  validates :nom, :prenom, :type_personne_id, :presence => true

  def nom_complet
    [prenom, nom].join(" ")
  end

  def nom_complet_type
  	[nom_complet, TypePersonne.all(:conditions => ['id = ?',type_personne_id]).first.libelle].join(" - ")
  end

  def type_personne
  	TypePersonne.find(type_personne_id).libelle
  end

end
